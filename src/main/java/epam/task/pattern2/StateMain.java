package epam.task.pattern2;

import epam.task.pattern2.model.Joba;

import java.util.Scanner;

public class StateMain {

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        String keyMenu = null;
        Joba joba = new Joba();
        System.out.println(joba.getState());
        do{
        // System.out.println(Color.DARK_GRAY);
         
            try {
                System.out.println("Enter command:");
                keyMenu = input.nextLine().toUpperCase();
                switch (keyMenu) {
                    case "1":
                        joba.goNextState();
                        break;
                    case "2":
                        joba.goPrevious();
                        break;
                    case "3":
                        joba.goStartState();
                        break;
                    default :
                        System.out.println("This is not a valid Menu Option! Please Select Another");
                        break;
                }
            }catch (Exception e){

            }

            System.out.println(joba.getState());
         }while (!keyMenu.equals("Q"));


   /*public abstract String vision();
    public abstract String userStories();
    public abstract String productBackLog();
    public abstract String selectedProductBackLog();
    public abstract String sprintBackLog();
    public abstract String newFunctionality();
    public abstract String sprintReview();
    public abstract String retrospective();
    public abstract String done();*/

    }

}
