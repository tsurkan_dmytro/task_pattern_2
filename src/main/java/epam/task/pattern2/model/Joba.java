package epam.task.pattern2.model;

import epam.task.pattern2.scrumstates.ScrumState;
import epam.task.pattern2.scrumstates.Vision;

public class Joba {

    private ScrumState state = new Vision();

    public void setState(ScrumState state)
    {
        this.state = state;
    }

    public ScrumState getState(){
        return state;
    }

    public void goNextState(){
        state.goNextState(this);

    };

    public void goPrevious(){
        state.goPrevious(this);
    };

    public void goStartState(){
        state.goStartState(this);

    };

}