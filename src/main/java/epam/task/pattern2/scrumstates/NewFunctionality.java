package epam.task.pattern2.scrumstates;

import epam.task.pattern2.model.Joba;

public class NewFunctionality implements ScrumState {

    @Override
    public String goNextState(Joba joba) {
        joba.setState(new SprintReview());
        return "Set Sprint Review State";
    }

    @Override
    public String goStartState(Joba joba) {
        return null;
    }

    @Override
    public String goPrevious(Joba joba) {
        return null;
    }


    public String toString(){
        return "NewFunctionality State";
    }
}
