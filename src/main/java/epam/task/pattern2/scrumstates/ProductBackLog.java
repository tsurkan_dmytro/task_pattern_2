package epam.task.pattern2.scrumstates;

import epam.task.pattern2.model.Joba;

public class ProductBackLog implements ScrumState {

    @Override
    public String goNextState(Joba joba) {
        joba.setState(new SelectedProductBackLog());
        return "Set Selected Product Back Log State";
    }

    @Override
    public String goStartState(Joba joba) {
        return "Can't run again";
    }

    @Override
    public String goPrevious(Joba joba) {
        return "Previous is blocked";
    }

    public String toString() {
        return "ProductBackLog State";
    }
}
