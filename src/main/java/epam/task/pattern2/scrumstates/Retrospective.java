package epam.task.pattern2.scrumstates;

import epam.task.pattern2.model.Joba;

public class Retrospective implements ScrumState {

    @Override
    public String goNextState(Joba joba) {
        joba.setState(new Done());
        return "Set Done State";
    }

    @Override
    public String goStartState(Joba joba) {
        return "Can't run again";
    }

    @Override
    public String goPrevious(Joba joba) {
        joba.setState(new SprintReview());
        return "Sprint Review State";
    }

    public String toString(){
        return "Retrospective State";
    }
}
