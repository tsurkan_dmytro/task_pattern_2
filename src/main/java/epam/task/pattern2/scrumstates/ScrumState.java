package epam.task.pattern2.scrumstates;

import epam.task.pattern2.model.Joba;

public interface ScrumState {

    public String goNextState(Joba joba);
    public String goStartState(Joba joba);
    public String goPrevious(Joba joba);


    /*default void vision() {
        System.out.println("Vision state isn't on ");
    }*/
    /*public abstract String userStories();
    public abstract String productBackLog();
    public abstract String selectedProductBackLog();
    public abstract String sprintBackLog();
    public abstract String newFunctionality();
    public abstract String sprintReview();
    public abstract String retrospective();
    public abstract String done();*/


}