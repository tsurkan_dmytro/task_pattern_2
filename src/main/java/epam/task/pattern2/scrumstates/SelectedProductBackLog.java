package epam.task.pattern2.scrumstates;

import epam.task.pattern2.model.Joba;

public class SelectedProductBackLog implements ScrumState {

    @Override
    public String goNextState(Joba joba) {
        joba.setState(new SprintBackLog());
        return "Set Sprint Back Log State";
    }

    @Override
    public String goStartState(Joba joba) {
        return null;
    }

    @Override
    public String goPrevious(Joba joba) {
        return "Previous is blocked";
    }

    public String toString(){
        return "SelectedProductBackLog State";}
}
