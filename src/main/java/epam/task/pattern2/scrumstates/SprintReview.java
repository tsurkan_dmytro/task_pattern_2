package epam.task.pattern2.scrumstates;

import epam.task.pattern2.model.Joba;

public class SprintReview implements ScrumState {

    @Override
    public String goNextState(Joba joba) {
        joba.setState(new Retrospective());
        return "Set Retrospective State";
    }

    @Override
    public String goStartState(Joba joba) {
        return null;
    }

    @Override
    public String goPrevious(Joba joba) {
        return null;
    }

    public String toString(){
        return "SprintReview State";
    }
}
