package epam.task.pattern2.scrumstates;

import epam.task.pattern2.model.Joba;

public class Vision implements ScrumState {

    @Override
    public String goStartState(Joba joba) {
        joba.setState(new Vision());
        return "Go to Vision";
    }

    @Override
    public String goPrevious(Joba joba) {
        return "This is root";
    }

    @Override
    public String goNextState(Joba joba) {
        joba.setState(new UserStories());
        return "Set User Stories State";
    }

    public String toString(){
        return "Vision State";
    }
}
